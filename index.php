<html>
  <head>
    <title>Super Design Attendance Sync</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-md-12" style="text-align: center; margin-top: 60px;">
          <h3>Super Design Attendance Sync</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4" style="text-align: center; margin-top: 60px;">
          <h1>Step 1.</h1>
          <button type="submit" class="generate">Generate List</button>
        </div>
        <div class="col-md-4" style="text-align: center; margin-top: 60px;">
          <h1>Step 2.</h1>
          <button type="submit" class="compare">Sync List</button>
        </div>
        <div class="col-md-4" style="text-align: center; margin-top: 60px;">
          <h1>Step 3.</h1>
          <a href="submit.php"><button>Run Marketo Form</button></a>
        </div>
      </div>      
    </div>
    <script type="text/javascript">
      jQuery('.generate').click(function() {
        jQuery.ajax({
          type: "POST",
          url: "scrape.php"
        }).done(function( msg ) {
          alert( "List Generated" );
        });
      });
    </script>
    <script type="text/javascript">
      jQuery('.compare').click(function() {
        jQuery.ajax({
          type: "POST",
          url: "compare.php"
        }).done(function( msg ) {
          alert( "CSV Synced" );
        });
      });
    </script>
  </body>
</html>