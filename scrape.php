<?php


$database_server = "localhost";
$database_username = "root";
$database_password = "root";
$database_name = "indesign_superdesign2020_wordpress";

$connection = mysqli_connect($database_server, $database_username, $database_password, $database_name);

if($connection === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

$query = "SELECT * FROM wp_tracking_data";
$result = mysqli_query($connection, $query);

$number_of_fields = mysqli_num_fields($result);
$headers = array();
for ($i = 0; $i < $number_of_fields; $i++) {
    $headers[] = mysqli_field_name($result , $i);
}
$current_date = date("Y-m-d_G-i-s");

$fp = fopen('csv/export_'.$current_date.'.csv', 'w');
if ($fp && $result) {
    while ($row = $result->fetch_array(MYSQLI_NUM)) {
        fputcsv($fp, array_values($row));
    }
    die;
}
fclose ($fp);

function mysqli_field_name($result, $field_offset)
{
    $properties = mysqli_fetch_field_direct($result, $field_offset);
    return is_object($properties) ? $properties->name : null;
}


CloseCon($conn);

?>