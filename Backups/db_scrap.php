<?php


 $database_server = "localhost";
 $database_username = "root";
 $database_password = "root";
 $database_name = "indesign_superdesign2020_wordpress";


/* Attempt MySQL server connection. */  
$connection = mysqli_connect($database_server, $database_username, $database_password, $database_name);

// Check connection
if($connection === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

function lineStart($file) {
    $position = ftell($file);
    while (fgetc($file) != "\n") {
        fseek($file, --$position);
        if ($position == 0) break;
    }
    return $position;
}

$yesterdayAdjust = strtotime("-1 day");
$yesterday = date("Y-m-d", $yesterdayAdjust);
$file = fopen('export_'.$yesterday.'.csv', 'r+');      

fseek($file, -1, SEEK_END); 

    lineStart($file);                    
    $line = fgetcsv($file);
    print_r( $line);

fclose($file);


$query = "SELECT * FROM wp_tracking_data";
$result = mysqli_query($connection, $query);

$number_of_fields = mysqli_num_fields($result);
$headers = array();
for ($i = 0; $i < $number_of_fields; $i++) {
    $headers[] = mysqli_field_name($result , $i);
}
$current_date = date("Y-m-d");

$fp = fopen('export_'.$current_date.'.csv', 'w');
if ($fp && $result) {
    //header('Content-Type: text/csv');
    //header('Content-Disposition: attachment; filename="export.csv"');
    //header('Pragma: no-cache');
    //header('Expires: 0');
    //fputcsv($fp, $headers);
    while ($row = $result->fetch_array(MYSQLI_NUM)) {
        fputcsv($fp, array_values($row));
    }
    die;
}
fclose ($fp);

function mysqli_field_name($result, $field_offset)
{
    $properties = mysqli_fetch_field_direct($result, $field_offset);
    return is_object($properties) ? $properties->name : null;
}


CloseCon($conn);

?>