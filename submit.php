<html>
    <head>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <title>Marketo Sync</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    </head>

<body>
    <div class="container">
      <div class="row">
        <div class="col-md-12" style="text-align: center; margin-top: 60px;">
          <h3>Marketo Sync</h3>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
<div class="autosubmit">
<form id="myform">
<?php 

function lineStart($file) {
    $position = ftell($file);
    while (fgetc($file) != "\n") {
        fseek($file, --$position);
        if ($position == 0) break;
    }
    return $position;
}

//$current_date = date("Y-m-d");
$files = scandir('csv_completed', SCANDIR_SORT_DESCENDING);
$newest_file = $files[0];
$file = fopen('csv_completed/'. $newest_file, 'r+'); 

fseek($file, -1, SEEK_END);             

while (fstat($file)['size']) {
    lineStart($file);                    
    $line = fgetcsv($file);
    print_r( $line);
    echo '<input name="user_email" value="'.$line[3].'">';
    echo '<input name="event_token" value="'.$line[4].'">';

    if ($line[6] == 'Live'){
    echo '<input name="watched_status" value="Attended">';
    } else {
    echo '<input name="watched_status" value="Attended On-Demand">';    
    }

    ftruncate($file, lineStart($file)); 
    break;
}

fclose($file);

?>
<button type="submit" class="submm">Submit</button>
</form>
</div>
</div>
</div>
    <div class="container">
      <div class="row">
<?php if (!empty($line)) { ?>
<script src="//app-sn05.marketo.com/js/forms2/js/forms2.min.js"></script>
<form id="mktoForm_1565" style="display:none"></form>
<script>MktoForms2.loadForm("//app-sn05.marketo.com", "401-UNZ-840", 1565);</script>
</div>
</div>
<script>
  jQuery(function ($) {

    'use strict';

    MktoForms2.whenReady(function(form){
      form.onSuccess(function(vals,thanksURL){
        return false;
      });
    });


    $(document).ready(function() {
      var $form = $(".autosubmit form");
      $form[0].addEventListener("submit", function () {
        var email = $form.find('input[name="user_email"]').val();
        var eventId = $form.find('input[name="event_token"]').val();
        var userStatus = $form.find('input[name="watched_status"]').val();
        var myForm_update = MktoForms2.allForms()[0];
        myForm_update.addHiddenFields({
          Email: email,
          cOB1String: eventId,
          cOB2String: userStatus,
        });
        myForm_update.submit();
      });
     });

     setTimeout(function() {
        $('.submm').click();
     }, 1500);
     $('#myform').submit(function(e) {
        e.preventDefault();
     }); 

     setTimeout(function() {
         window.location.reload(true);
     }, 4000);

  });
</script>
<style>
  #mktoForm_1565{ display: block !important;  }
  </style>
<?php } else { echo 'File Completed';} ?>
</body>
</html>