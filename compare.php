<?php 

function row_compare($a, $b)
{
    if ($a === $b) {
        return 0;
    }

    return (implode("",$a) < implode("",$b) ) ? -1 : 1;
}

$current_date = date("Y-m-d_G-i-s");

$files = scandir('csv', SCANDIR_SORT_DESCENDING);
$newest_file = $files[0];
$past_file = $files[1];

$file1 = new SplFileObject('csv/'.$past_file);
$file1->setFlags(SplFileObject::READ_CSV);

$file2 = new SplFileObject('csv/'.$newest_file);
$file2->setFlags(SplFileObject::READ_CSV);


foreach ($file1 as $row) {
    $csv_1[] = $row;
}

foreach ($file2 as $row) {
    $csv_2[] = $row;
}

$unique_to_csv1 = array_udiff($csv_1, $csv_2, 'row_compare');
$unique_to_csv2 = array_udiff($csv_2, $csv_1, 'row_compare');

$all_unique_rows = array_merge($unique_to_csv1,$unique_to_csv2);

$fp = fopen('csv_completed/submitted.'.$current_date.'.csv', 'w');
$fp_archive = fopen('csv_archive/submitted.'.$current_date.'.csv', 'w');

foreach($all_unique_rows as $unique_row) {
    fputcsv($fp, $unique_row);
    fputcsv($fp_archive, $unique_row);
}

fclose ($fp);
fclose ($fp_archive);


?>